var canvas = document.getElementById('myCanvas');
var ctx = canvas.getContext('2d');
var draw = false;//if draw or not
var tempPos = [0, 0];//start position
var color = document.getElementById('color');
var lineWidth = document.getElementById('lineWidth');
var use_brush = true;
//eraser variable
var eraser = document.getElementById('eraser');
var eraserWidth = document.getElementById('eraserWidth');
var StateArray = new Array();
var State = -1;
Pushing();

//Text
var text_value="Arial";
var text_input;
//draw diagram
var draw_tri;
var draw_squ;
var draw_cir;
var eraser_click;
var x = 0, y = 0;
var textx = 0; texty = 0;
ctx.strokeStyle = color.value;
ctx.lineWidth = lineWidth.value;
var burshCap="butt";
var savedImageData;
let fillColor = 'black';
var fillshape = document.getElementById("myCheck");


// Hold the position where clicked;
class MouseDownPos{
    constructor(x, y){
        this.x = x;
        this.y = y;
    }
}

//Hold the location of mouse
class Location{
    constructor(x, y){
        this.x = x;
        this.y = y;
    }
}

class ShapeBounding{
    constructor(left, top, width, height){
        this.left = left;
        this.top = top;
        this.width = width;
        this.height = height;
    }
}

let shapeBoundingBox = new ShapeBounding(0,0,0,0);
let mousedown = new MouseDownPos(0,0);
let loc = new Location(0,0);

function SaveCanvasImage(){
    savedImageData = ctx.getImageData(0,0,500,500);
}

function RedrawCanvasImage(){
    ctx.putImageData(savedImageData,0,0);
}

function UpdateGraphSizeData(loc){
    shapeBoundingBox.width = Math.abs(loc.x - mousedown.x);
    shapeBoundingBox.height = Math.abs(loc.y - mousedown.y);

    if (loc.x > mousedown.x){
        shapeBoundingBox.left = mousedown.x;
    }
    else{
        shapeBoundingBox.left = loc.x;
    }
    if (loc.y > mousedown.y){
        shapeBoundingBox.top = mousedown.y;
    }
    else{
        shapeBoundingBox.top = loc.y;
    }
}

function drawGraphShape(loc){
    ctx.fillStyle = fillColor;
    if(draw_squ){
        if (fillshape.checked){
            ctx.fillRect(shapeBoundingBox.left, shapeBoundingBox.top, shapeBoundingBox.width, shapeBoundingBox.height);
        }
        else{
            ctx.strokeRect(shapeBoundingBox.left, shapeBoundingBox.top, shapeBoundingBox.width, shapeBoundingBox.height);
        }
    }
    else if(draw_cir){
        if(fillshape.checked){
            let radius = shapeBoundingBox.width;
            ctx.beginPath();
            ctx.arc(mousedown.x, mousedown.y, radius, 0, Math.PI*2);
            ctx.fill();
        }
        else{
            let radius = shapeBoundingBox.width;
            ctx.beginPath();
            ctx.arc(mousedown.x, mousedown.y, radius, 0, Math.PI*2);
            ctx.stroke();
        }
    }
    else if(draw_tri){
        if(fillshape.checked){
            ctx.beginPath();
            ctx.lineTo(loc.x,mousedown.y);
            ctx.lineTo(mousedown.x,mousedown.y);
            ctx.lineTo(loc.x,loc.y);
            ctx.fillStyle = ctx.strokeStyle;
            ctx.fill();
            ctx.closePath(); 
        }
        else{
            ctx.beginPath();
            ctx.lineTo(loc.x,mousedown.y);
            ctx.lineTo(mousedown.x,mousedown.y);
            ctx.lineTo(loc.x,loc.y);
            ctx.closePath();
            ctx.stroke();
        }
    }
}

function UpdateGraphOnMove(loc){
    UpdateGraphSizeData(loc);
    drawGraphShape(loc);
}

document.getElementById("myCanvas").style.cursor="crosshair";

color.addEventListener('input', function(){
    ctx.strokeStyle = color.value;
})

lineWidth.addEventListener('input', function(){
    ctx.lineWidth = lineWidth.value;
})


canvas.addEventListener('mousedown', function(e){
    tempPos = [e.pageX-canvas.offsetLeft, e.pageY-canvas.offsetTop];
    loc.y = e.pageY-canvas.offsetTop;
    loc.x = e.pageX-canvas.offsetLeft;
    mousedown.x = loc.x;
    mousedown.y = loc.y;
    textx = mousedown.x;
    texty = mousedown.y;
    console.log(fillshape.checked);
    
    if(text_input){
        var temp="px ";
        temp=temp+text_value;
        ctx.font=document.getElementById("text_size").value+temp;
        ctx.fillStyle=color.value;
        ctx.fillText(document.getElementById("textin").value,mousedown.x,mousedown.y,500-x);
    }
    else{
    SaveCanvasImage();
    draw = true;
    }
    
});


canvas.addEventListener('mousemove', function(e){
    if(draw){
        var newPos = [e.pageX - canvas.offsetLeft, e.pageY - canvas.offsetTop];
        loc.x = e.pageX - canvas.offsetLeft;
        loc.y = e.pageY - canvas.offsetTop;
        if(eraser_click){
            //document.getElementById("myCanvas").style.cursor="cell";
            var _eraserWidth = eraserWidth.value;
            ctx.clearRect(tempPos[0]-(_eraserWidth/2), tempPos[1]-(_eraserWidth/2), _eraserWidth, _eraserWidth);
        }
        else if(draw_tri){
            RedrawCanvasImage();
            UpdateGraphOnMove(loc);
        }

        else if(draw_squ){
            console.log(draw_squ);
            RedrawCanvasImage();
            UpdateGraphOnMove(loc);
        }

        else if(draw_cir){
            RedrawCanvasImage();
            UpdateGraphOnMove(loc);
        }

        else if(use_brush){
            ctx.beginPath();
            ctx.lineCap=burshCap;
            ctx.moveTo(tempPos[0], tempPos[1]);
            ctx.lineTo(newPos[0], newPos[1]);
            //ctx.closePath();
            ctx.stroke();
        }
        
        tempPos = newPos;

    }
});

canvas.addEventListener('mouseup', function(){
    ctx.closePath();
    
    draw = false;
    if(draw_squ){
        RedrawCanvasImage();
        UpdateGraphOnMove(loc);
    }
    else if(draw_cir){
        RedrawCanvasImage();
        UpdateGraphOnMove(loc);
    }
    else if(draw_tri){
        RedrawCanvasImage();
        UpdateGraphOnMove(loc);
    }
    //draw = false;
    Pushing();
});
/*
document.addEventListener("keydown", function(e){
    ctx.font = "16px Arial";

    if(e.keyCode === 13){
        //Enter key is pressed
        textx = mousedown.x;
        texty += 20;
    }else{
        ctx.fillText(e.key, textx, texty);
        textx += ctx.measureText(e.key).width;
    }
    
});
*/


function reset(){
    ctx.clearRect(0, 0, 500, 500);
    Pushing();
}

function Pushing(){
    if(State<StateArray.length){
        StateArray.length=State+1;
    }
    State++;
    StateArray.push(ctx.getImageData(0,0,canvas.width,canvas.height));
}

function Undo(){
    if(State>0){
        State--;
        ctx.putImageData(StateArray[State],0,0);
    }
}

function Redo(){
    if(State<StateArray.length-1){
        State++;
        ctx.putImageData(StateArray[State],0,0);
    }
}

function undo_click(){
    Undo();
}

function redo_click(){
    Redo();
}

function draw_triangle(){
    draw_tri = true;
    draw_squ = false;
    draw_cir = false;
    eraser_click = false;
    use_brush = false;
    text_input=false;
    document.getElementById("myCanvas").style.cursor="ne-resize";
}

function draw_square(){
    draw_tri = false;
    draw_squ = true;
    draw_cir = false;
    eraser_click = false;
    use_brush = false;
    text_input=false;
    document.getElementById("myCanvas").style.cursor="move";
}

function draw_circle(){
    draw_tri = false;
    draw_squ = false;
    draw_cir = true;
    use_brush = false;
    eraser_click = false;
    text_input=false;
    document.getElementById("myCanvas").style.cursor="grab";
}

function Eraser_click(){
    draw_tri = false;
    draw_squ = false;
    draw_cir = false;
    eraser_click = true;
    text_input=false;
    use_brush = false;
    document.getElementById("myCanvas").style.cursor="cell";
}

function Brush_click(){
    draw_tri = false;
    draw_squ = false;
    draw_cir = false;
    eraser_click = false;
    use_brush = true;
    document.getElementById("myCanvas").style.cursor="haircross";
    text_input=false;
    burshCap="butt";
}


function text_type_click(){
    text_value=document.getElementById("fontstlye").value;
}

function text_click(){
    text_input=true;
    draw_tri = false;
    draw_squ = false;
    draw_cir = false;
    eraser_click = false;
    use_brush = false;
    document.getElementById("myCanvas").style.cursor="text";
}

function previewImage(input){
    var reader = new FileReader();
    reader.onload = function(e){
        document.getElementById("preview").setAttribute("src", e.target.result);
    };
    reader.readAsDataURL(input.files[0]);
}

function setImage(){
    var x = document.getElementById("x").value;
    var y = document.getElementById("y").value;
    var width = document.getElementById("width").value;
    var height = document.getElementById("height").value;

    var image = document.getElementById("preview");
    ctx.drawImage(image, x, y, width, height);
}

function roundhead_click(){
    burshCap = "round";
}

function squarehead_click(){
    burshCap = "square";
}