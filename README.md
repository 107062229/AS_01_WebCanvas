# Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**             # Software Studio 2020 Spring
## Assignment 01 Web Canvas


### Scoring

| **Basic components**                             | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Basic control tools                              | 30%       | Y         |
| Text input                                       | 10%       | Y         |
| Cursor icon                                      | 10%       | Y         |
| Refresh button                                   | 10%       | Y         |

| **Advanced tools**                               | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Different brush shapes                           | 15%       | Y         |
| Un/Re-do button                                  | 10%       | Y         |
| Image tool                                       | 5%        | Y         |
| Download                                         | 5%        | Y         |

| **Other useful widgets**                         | **Score** | **Check** |
| :----------------------------------------------- | :-------: | :-------: |
| Name of widgets                                  | 1~5%     | N         |


---

### How to use 

    點Brush可以使用筆刷，旁邊可以選顏色跟粗細。點Eraser可以用橡皮擦，旁邊可以調整大小。再來就是Redo、Undo、Reset。
    然後Triangle、Square、Circle畫圖形。
    按download可以下載當前圖片，可以在旁編書入檔名。
    在Add Text 旁邊的文字框輸入文字，再點Add Text後，在畫布想要的位置點擊滑鼠，就可加入文字，旁邊選擇字型、大小。
    下方的選擇檔案，可以插入圖片，輸入想加入的位置跟寬高，點選Set image來加入圖片

### Function description

    Decribe your bouns function and how to use it.

### Gitlab page link

    https://107062229.gitlab.io/AS_01_WebCanvas 

### Others (Optional)

    Anythinh you want to say to TAs.

<style>
table th{
    width: 100%;
}
</style>